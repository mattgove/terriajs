# README #

A series of simple tutorials for automating tasks and processes in TerriaJS.

### What is this repository for? ###

This repository is a tutorial for all individuals, teams, and organizations who are interested in learning Python. It is designed for educational purposes.

### How do I get set up? ###

1. Clone the repository to your computer 
2. Open a Terminal or Command Prompt Window
3. Navigate (`cd`) to the cloned repository
4. Run the script by executing `python3 [name-of-script].py`

### Who do I talk to? ###

This repository is owned by Matthew Gove Web Development, LLC. Please [contact us](https://www.matthewgove.com/contact-me/) with any question, comments, or concerns you may have.

### Licensing ###

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses).
