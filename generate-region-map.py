#!/usr/bin/env python3
import json
import os

def fid(f):
    return f["properties"]["FID"]


############ User Options #############
GEOJSON = "world-countries.geojson"
LAYER = "WORLD_COUNTRIES"
MAPPING_ID_PROPERTY = "alpha3code"
OUTPUT_FOLDER = "regionIds"
#######################################



# Generate output file name
geojson_filename = GEOJSON.replace(".geojson", "")
output_json_filename = "region_map-{}.json".format(geojson_filename)
output_fpath = "{}/{}".format(OUTPUT_FOLDER, output_json_filename)

# Load Data from GeoJSON
with open(GEOJSON, "r") as infile:
    raw_json = json.load(infile)

# Sort the GeoJSON features by FID
raw_features = raw_json["features"]
features = sorted(raw_features, key=fid)

region_mapping_ids = []

for f in features:
    properties = f["properties"]
    region_map_id = properties[MAPPING_ID_PROPERTY]
    region_mapping_ids.append(region_map_id)

# Output to JSON Format
if not os.path.isdir(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)

output_json = {
    "layer": LAYER,
    "property": MAPPING_ID_PROPERTY,
    "values": region_mapping_ids
}

with open(output_fpath, "w") as ofile:
    json.dump(output_json, ofile, indent=4)

print("Successfully wrote region map for {} to {}!".format(GEOJSON, output_json_filename))
